<?php

use Dotenv\Environment\DotenvFactory;

function loadCwpConfig($appName)
{
    $envToLoad = [
        '.env' => realpath(__DIR__ . '/../../../../'),
        '.env.' . $appName => realpath(__DIR__ . '/../../../../')
    ];

    foreach ($envToLoad as $filename => $path) {
        if (!file_exists($path . '/' . $filename)) {
            $path =  realpath($path . '/../');
        }

        try {
            (Dotenv\Dotenv::create($path, $filename))->load();
        } catch (Dotenv\Exception\InvalidPathException $e) {

        }
    }

    $ssEnvLocation = __DIR__ . '/../../../../_ss_environment.php';

    $ssEnvFound = false;

    if (file_exists($ssEnvLocation)) {
        require_once $ssEnvLocation;
        $ssEnvFound = true;
    } else {
        $ssEnvLocation = __DIR__ . '/../../../../../_ss_environment.php';

        if (file_exists($ssEnvLocation)) {
            require_once $ssEnvLocation;
            $ssEnvFound = true;
        }
    }

    if ($ssEnvFound) {
        $configToShare = [
            'SS_DATABASE_SERVER' => 'DB_HOST',
            'SS_DATABASE_PORT' => 'DB_PORT',
            'SS_DATABASE_NAME' => 'DB_DATABASE',
            'SS_DATABASE_USERNAME' => 'DB_USERNAME',
            'SS_DATABASE_PASSWORD' => 'DB_PASSWORD',
            'WKHTMLTOPDF_BINARY' => 'WKHTML_PDF_BIN_PATH'
        ];

        $loader = new Dotenv\Loader([], new DotenvFactory());

        foreach ($configToShare as $ssConfigKey => $envConfigKey) {
            if (defined($ssConfigKey)) {
                $loader->setEnvironmentVariable($envConfigKey, constant($ssConfigKey));
            }
        }
    }
}
